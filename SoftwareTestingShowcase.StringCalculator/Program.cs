﻿using SoftwareTestingShowcase.BestOneOpulentBrutalSystem.Adapters;
using System;
using System.Linq;

namespace SoftwareTestingShowcase.BestOneOpulentBrutalSystem
{
    public class Program
    {
        static void Main()
        {
            var adapter = new BuggyIntegratorAdapter();
            foreach (var person in adapter.GetPeople())
            {
                Console.WriteLine(person);
            }
        }
    }

}