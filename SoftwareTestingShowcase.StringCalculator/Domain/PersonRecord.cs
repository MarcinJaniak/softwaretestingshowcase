﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoftwareTestingShowcase.BestOneOpulentBrutalSystem.Domain
{
    public class PersonRecord
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int AccountSum { get; set; }

        public override string ToString()
        {
            return $"{FirstName} {LastName} got {AccountSum.ToString()}";
        }
    }
}
