﻿using SoftwareTestingShowcase.BestOneOpulentBrutalSystem.Domain;
using SoftwareTestingShowcase.BuggyIntegrator.Integrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftwareTestingShowcase.BestOneOpulentBrutalSystem.Adapters
{
    public class BuggyIntegratorAdapter
    {
        private BuggyIntegrator.Integrator.BuggyIntegrator _integrator { get; set; }
        public BuggyIntegratorAdapter()
        {
            _integrator = new BuggyIntegrator.Integrator.BuggyIntegrator();
        }
        public IEnumerable<PersonRecord> GetPeople()
        {
            return _integrator.GetPeople().Select(x => new PersonRecord
            {
                FirstName = x.FirstName,
                LastName = x.LastName,
                AccountSum = x.AccountSum
            });
        }

    }
}
