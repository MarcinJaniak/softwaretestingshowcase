﻿using NUnit.Framework;
using SoftwareTestingShowcase.BuggyIntegrator.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SoftwareTestingShowcase.BuggyIntegrator.UnitTests
{
    [TestFixture]
    public class StringCalculatorTests
    {
        //[SetUp]
        //[TearDown]

        [Test]
        [TestCase("1,2, 3", 6)]
        [TestCase("2, 2", 4)]
        public void StringCalculator_ValidInputTest_ReturnsValidResult(string input, int expected)
        {
            //arrange
            var stringCalculator = new StringCalculator();

            //act
            var actual = stringCalculator.Add(input);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource(typeof(MyDataClass), "TestCases")]
        public int CASESOURCE_StringCalculator_ValidInputTest_ReturnsValidResult(string input)
        {
            //arrange
            var stringCalculator = new StringCalculator();

            //act
            var actual = stringCalculator.Add(input);

            //assert
            return actual;
        }

        [Test]
        public void StringCalculator_NegativeInput_ThrowsOutOfRangeException()
        {
            //arrange
            var stringCalculator = new StringCalculator();

            //assert
            Assert.Throws<ArgumentOutOfRangeException>(() => { stringCalculator.Add("1, -2, 3"); });
        }
    }

    public class MyDataClass
    {
        public static IEnumerable TestCases
        {
            get
            {
                yield return new TestCaseData("1,2,3,4").Returns(10);
                yield return new TestCaseData("3, 2").Returns(5);
                yield return new TestCaseData("3, 2").Returns(5);
            }
        }
    }
}
