﻿using SoftwareTestingShowcase.EnourmousNotOptimalVileAccountings.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftwareTestingShowcase.EnourmousNotOptimalVileAccountings.DataProvider
{
    public class PersonProvider
    {
        public IEnumerable<Person> GetPeople()
        {
            return Enumerable.Empty<Person>().Concat(new[]
            {
                new Person {Fullname = "John Doe", AccountHistory="1, 2, 4, 4" },
                new Person {Fullname = "John Doe2", AccountHistory="1, 2, 5, 4" },
                new Person {Fullname = "John Doe7", AccountHistory="1, 2, 6, 4" },
                new Person {Fullname = "John Doe6", AccountHistory="1, 2, 7, 4" },
                new Person {Fullname = "John Doe5", AccountHistory="1, 2, 8, 4" },
                new Person {Fullname = "John Doe4", AccountHistory="1, 2, 9, 4" },
                new Person {Fullname = "John Doe3", AccountHistory="1, 2, 10, 4" },
                new Person {Fullname = "John Doe1", AccountHistory="1, 2, 11, 4" },
                new Person {Fullname = "John Boe", AccountHistory="1, 2, 12, 4" },
                new Person {Fullname = "John Aoe", AccountHistory="1, 2, 35, 4" },
                new Person {Fullname = "JohnAoe", AccountHistory="1, 2, 31, 4" },
                new Person {Fullname = "ThreePartName1st 2nd 3rd", AccountHistory="1, 2, 3, 4" },
                new Person {Fullname = "Johny Noe", AccountHistory="1, 2, 3, -4" },
                new Person {Fullname = "Jack Dack", AccountHistory="1, 2, , 3" },
                new Person {Fullname = "Becky White", AccountHistory="/;\n1,\n2\n,3" },
                new Person {Fullname = "Jacky Wacky", AccountHistory="//;\n1,\n2\n,3" },
                new Person {Fullname = "JackyWacky", AccountHistory="//;\n1,\n2\n,3" },
                new Person {Fullname = null, AccountHistory="//;\n1,\n2\n,3" },
                new Person {Fullname = null, AccountHistory=null },
                new Person {Fullname = "Jack2y Wacky", AccountHistory=null },
            }).OrderBy(x => Guid.NewGuid()).Take(3);
        }
    }
}
