﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftwareTestingShowcase.BuggyIntegrator.Services
{
    public interface ICalculatorService<T>
    {
        int Add(T input);
    }

    public class StringCalculator : ICalculatorService<string>
    {
        public int Add(string input)
        {
            var delimeter = input.StartsWith('/') ? input.Substring(1, input.IndexOf('\n')) : ",";
            var result = 0;
            var numbers = input.Split(delimeter)
                               .SelectMany(n => n.Split('\n'))
                               .Select(no =>
                               {
                                   if (int.TryParse(no, out var parseResult))
                                   {
                                       return parseResult > 0 ? parseResult : throw new ArgumentException();
                                   }
                                   else
                                   {
                                       throw new ArgumentOutOfRangeException();
                                   }
                               });

            result = numbers.Sum();

            return result;
        }
    }
}
