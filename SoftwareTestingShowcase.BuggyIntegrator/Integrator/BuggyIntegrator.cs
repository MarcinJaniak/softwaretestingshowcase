﻿using SoftwareTestingShowcase.BuggyIntegrator.Dto;
using SoftwareTestingShowcase.BuggyIntegrator.Services;
using SoftwareTestingShowcase.EnourmousNotOptimalVileAccountings.DataProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftwareTestingShowcase.BuggyIntegrator.Integrator
{
    public class BuggyIntegrator
    {
        public IEnumerable<PersonDto> GetPeople()
        {
            var stringCalculator = new StringCalculator();
            var personProvider = new PersonProvider();

            return personProvider.GetPeople()
                .Select(x =>
                {
                    try
                    {
                        return new PersonDto
                        {
                            FirstName = x.Fullname.Split(' ')[0],
                            LastName = x.Fullname.Split(' ')[1],
                            AccountSum = stringCalculator.Add(x.AccountHistory)
                        };
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                });
        }
    }
}
