﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoftwareTestingShowcase.BuggyIntegrator.Dto
{
    public class PersonDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int AccountSum { get; set; }
    }
}
